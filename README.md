# calorie Estimation

This Project is intended for food detection and classification based on the Mask RCNN model. 
### Requirements 
```
  python3
  pycocotools
  matplotlib 
  tensorflow==1.14.0
  keras==2.0.8
  mrcnn
  tqdm
  numpy
  pylab
  skimage
  notebook
  scipy
  Pillow
  cython
  scikit-image
  opencv-python
  h5py
  imgaug
```
### Note: installation for mrcnn 
```
 git clone https://github.com/matterport/Mask_RCNN.git
 cd Mask_RCNN
 python setup.py install	
 cd ../
 rm -rf Mask_RCNN
```
### Structure
- dataset (contain train and test images)
- logs: store the intermediate/checkpoints and final weights after training
- weights: weights for the model, we fetch the weights from here for the test script
- calorie_estimation.ipynb: main script for this section. find details below

### How to run 
 Run jupyter-notebook 
 --you can run all cells 

or run one by one
- Import libs
- Define root directory of project, model and logs
- Define all classe of mask RCNN moodel and adding our specific class
- class for training Confuration 
- class for load images, create mask and generate inference 
- call class config class
- call class datatset class
- Training
- Evaluating of food

	

